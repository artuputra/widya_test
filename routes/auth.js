const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const User = mongoose.model("User")
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { JWT_SECRET } = require('../keys')
const requireLogin = require('../middleware/requireLogin')

router.post('/signup',(req,res)=>{
    console.log(req.body.name)
    const {name,email,password} = req.body
    if (!email || !password || !name){
        return res.status(422).json({error:"please add all the fields"})
    }
    // res.json({message:"successfully post data"})
    User.findOne({email:email})
    .then((saveduser)=>{
        if(saveduser){
            return res.status(422).json({error:"user alredy exist with that email"})
        }
        bcrypt.hash(password,12)
        .then(hashedpassword=>{
            const user = new User({
                email,
                password:hashedpassword,
                name
            })
            user.save()
            .then(user=>{
                res.json({message:"saved successfully"})
            })
            .catch(err=>{
                console.log(err)
            })
        })
    })
    .catch(err=>{
        console.log(err)
    })
})

router.post('/signin',(req,res)=>{
    const {email,password} = req.body
    if(!email||!password){
        return res.status(422).json({error:"please fill email or password"})
    }
    User.findOne({email:email})
    .then(saveduser=>{
        if(!saveduser){
            return res.status(422).json({error:"invalid email or password"})
        }
        bcrypt.compare(password,saveduser.password)
        .then(doMatch=>{
            if(doMatch){
                // return res.json({message:"You have been signed in"})
                const token = jwt.sign({_id:saveduser._id},JWT_SECRET)
                res.json({token})
            }
            else{
                return res.status(422).json({error:"invalid email or password"})
            }
        })
        .catch(err=>{
            console.log(err)
        })
    })
})

router.get('/verify',requireLogin,(req,res)=>{
    res.send('oke')
})

router.get('',(req,res)=>{
    return res.json({message:"Hallo"})
})

module.exports = router