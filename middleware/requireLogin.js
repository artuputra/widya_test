const jwt = require('jsonwebtoken')
const {JWT_SECRET} = require('../keys')
const mongoose = require('mongoose')
const User = mongoose.model('User')

module.exports = (req,res,next)=>{
    const {authorization} = req.headers
    if(!authorization){
        console.log(req.body)
        return res.status(401).json({error:"You need to login"})
    }
    const token = authorization.replace("Bearer ","")
    jwt.verify(token,JWT_SECRET,(err,payload)=>{
        if(err){
            console.log(token)
            return res.status(401).json({error:"You need to login guys"})
        }
        const {_id}=payload
        User.findById(_id).then(userdata=>{
            req.User = userdata
        })
        next()
    })
}