const express = require('express')
const app = express()
const mongoose = require('mongoose')
const {MONGOURI} = require("./keys")
const PORT = 5000

mongoose.connect(MONGOURI,{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
mongoose.connection.on('connected',()=>{
    console.log("connected on database")
})
mongoose.connection.on('error',()=>{
    console.log("error to connect database")
})

require("./models/user")
require("./models/post")

app.use(express.json())
app.use(require('./routes/auth'))
app.use(require('./routes/post'))


app.listen(PORT,'0.0.0.0',()=>{
    console.log("server running on ",PORT)
})