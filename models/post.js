const mongooose = require('mongoose')
const {ObjectId} = mongooose.Schema.Types
var datetime = new Date()
var date_now = datetime.toISOString().slice(0,10)

const postSchema = new mongooose.Schema({
    title:{
        type:String,
        required:true
    },
    body:{
        type:String,
        required:true
    },
    published_at:{
        type:Date,
        default:date_now
    },
    postedBy:{
        type:ObjectId,
        ref:"User"
    }
})

mongooose.model("Post",postSchema)